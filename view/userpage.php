<?php session_start(); ?>
<html>
<head>
<title>Quiz page</title>
</head>
<body>
    <?php 
        if (!isset($_SESSION['email'])) {
            header("Location:../index.php?log=please login first");
        }

        if(isset($_GET['done'])) {
            echo "<script type='text/javascript'>alert('$_GET[done]')</script>";
        }
    ?>

    <form action="../include/session.php" method="post">
        <input type="submit" value="logout" name="logout">
    </form>
    <a href="test.php" title="start test">start test</a>
</body>
</html>