<?php 
session_start();

@include "../controller/testcontroll.php";


if (!isset($_SESSION['email'])) {
    header("Location:../index.php?log=please login first");
}

if (isset($_SESSION['result'])) {
    header("Location:../view/result.php");
}

if (isset($_GET['empty'])) {
    echo "<script type='text/javascript'>alert('$_GET[empty]')</script>";
}

?>



<html>
<head>
<title>
test page
</title>
</head>
<body>
<form action="../include/test.inc.php" method="post">
<?php 
$rec = $ques->check();
$count = 1;
foreach($rec as $values) {
    ?>
    <div>
    <p><?php echo $values['question'] ?></p>
    <label for="ans1"><?php echo $values['opt_one'] ?></label>
    <input type="radio" name='ans<?php echo $count?>' id="ans1" value="<?php echo $values['opt_one'] ?>">
    <label for="ans2"><?php echo $values['opt_two'] ?></label>
    <input type="radio" name='ans<?php echo $count?>' id="ans2" value="<?php echo $values['opt_two'] ?>">
    <label for="ans3"><?php echo $values['opt_three'] ?></label>
    <input type="radio" name='ans<?php echo $count?>' id="ans3" value="<?php echo $values['opt_three'] ?>">
    <label for="ans4"><?php echo $values['opt_four'] ?></label>
    <input type="radio" name='ans<?php echo $count?>' id="ans4" value="<?php echo $values['opt_four'] ?>">
    </div>
    <?php
    $count++;
}

?>

<input type="submit" value="submit" name="submit">
</form>
</body>
</html>