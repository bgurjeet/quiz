<?php 

@include "connection.php";

class Test extends connection
{
    public function check()
    {   
        try {

            $question = [];
            $query = "SELECT * from quiz";
            $data = $this->conn->prepare($query);
            $data->execute();
            while($row = $data->fetch(PDO::FETCH_ASSOC)) {
                $question[] = $row;
            }
            return $question;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function ans()
    {   
        try {

            $answer = [];
            $query = "SELECT correct from quiz";
            $val = $this->conn->prepare($query);
            $val->execute();
            while($row = $val->fetch(PDO::FETCH_ASSOC)) {
                $answer[] = $row;
            }
            return $answer;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

$ques = new Test();
$ques->ans();
