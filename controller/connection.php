<?php 
class Connection 
{
    private $localhost = 'localhost';
    private $Username = 'root';
    private $password = 'root';
    private $database = 'quiz';

    public function __construct() {
        try {
            $this->conn = new PDO("mysql:host = $this->localhost;dbname=$this->database",$this->Username,$this->password);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

