<?php 

include_once "connection.php";
class Customers extends Connection
{
    
    public function login($email,$password)
    {
        try {
            $data = "SELECT * FROM user WHERE email = ? AND password = ?";
            $val = $this->conn->prepare($data);
            $val->execute([$email,$password]);
            if($val->rowcount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function register($name, $email, $password,$score,$count) 
	{
        try  {
            $val = 'SELECT * FROM user WHERE email = ?';
            $stmt = $this->conn->prepare($val);
            $stmt->execute([$email]);
            if($stmt->rowcount() > 0) {
                return false;
            } else {
                $query = "INSERT into user(name,email,password,score,counter) values (?,?,?,?,?)";
                var_dump($query);
                $data = $this->conn->prepare($query);
                $data->execute([$name,$email, $password,$score,$count]);
                if ($data) {
                    return true;
                } else {
                    return false;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
        
    public function update($name,$score) 
    {
        try {
            $query = "UPDATE `user` SET `score`=? WHERE email = ?";
            $up = $this->conn->prepare($query);
            $up->execute([$score,$name]);
            if ($up) {
                return true;
            } else {
                return false;
            } 
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function result()
    {   
        try {
            $records = [];
            $data = "SELECT * FROM user ORDER BY score DESC";
            $val = $this->conn->prepare($data);
            $val->execute();
            while($row = $val->fetch(PDO::FETCH_ASSOC)) {
                $records[] = $row;
            }
            return $records;
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function find($email)
    {
        try {
            $counter = "SELECT counter from user where email = ?";
            $data = $this->conn->prepare($counter);
            $data->execute($email);
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function upcount($val,$email)
    {
        try {
            $counter = "UPDATE `user` SET `counter`= ? WHERE email = ?";
            $data = $this->conn->prepare($counter);
            $data->execute([$val,$email]);
            if ($data) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}

$obj = new Customers();
$obj->upcount(1,'gurjeetb@prdxn.com');


