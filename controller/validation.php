<?php 

@include 'connection.php';
class validator extends Connection
{

    public function test($data)
    {
        try {
            
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            
            return $data;
        } catch(PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function namevalidation($name) 
    {
        try {

            if (empty($name)) { 
                echo "wrong";
                return false;
            } else {
                $name = $this->test($name);
                if (!(preg_match('/^[a-zA-Z ]*$/',$name))) {
                    echo "wrong";
                    return false;
                } else {
                    echo "right";
                    return true;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function emailvalidation($email) 
    {
        try {
            if (empty($email)) {
                return false;
            } else {
                $email = $this->test($email);
                if (!filter_var($email,FILTER_VALIDATE_EMAIL)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function passwordvalidation($password)
    {
        try {

            if(empty($password)) {
                return false;
            } else {
                $password = $this->test($password);
                if (!preg_match('/^.{8,}$/',$password)) {
                    return false;
                } else {
                    return true;
                }
            }
        } catch (PDOException $e) {
            echo $e->getMessage();
        }
    }
}


$valid = new validator();

