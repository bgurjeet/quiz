<!doctype html>
<!-- If multi-language site, reconsider usage of html lang declaration here. -->
<html lang="en"> 
<head>
	<meta charset="utf-8">
	<!-- Setting the viewport for Media Query -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0">
	<title>Framework</title>
	<!-- Place favicon.ico in the root directory: mathiasbynens.be/notes/touch-icons -->
	<link rel="shortcut icon" href="favicon.ico" />	
	<!-- Adding reference to font awesome -->
	<link rel="stylesheet" href="assets/vendor/font/fontawesome-all.css">
	<!-- Default style-sheet is for 'media' type screen (color computer display).  -->
	<link rel="stylesheet" media="screen" href="assets/css/style.css">
</head>
<body>
	<div class="container">
	<?php 
    session_start();
    if (isset($_SESSION['email'])) {
        header("Location:view/userpage.php");
	}
	
	if (isset($_GET['log'])) {
        echo "<script type='text/javascript'>alert('$_GET[log]')</script>";
    }

    if (isset($_GET['played'])) {
        echo "<script type='text/javascript'>alert('$_GET[played]')</script>";
    }

    if (isset($_GET['error'])) {
        echo "<script type='text/javascript'>alert('$_GET[error]')</script>";
    }

    if (isset($_GET['error1'])) {
        echo "<script type='text/javascript'>alert('$_GET[error1]')</script>";
    }

    if (isset($_GET['done'])) {
        echo "<script type='text/javascript'>alert('$_GET[done]')</script>";
    }


    function data($value) {
        if (isset($_GET[$value])) {
            echo $_GET[$value];
        }
    }

    ?>
    <form action="include/login.inc.php" method="post">
    <div>
        <input type="text" name="email" placeholder="please enter your email" value="<?php  data('val2') ?>" >
        <span>
        <?php 
            if (isset($_GET['email'])) {
                echo $_GET['email'];
            }
        ?>
        </span>
        <span>
        <?php 
        if (isset($_GET['Message'])) {
            echo $_GET['Message'];
        }
    ?>
    </span>
    </div>
    <div>
        <input type="password" name="password" placeholder="please enter your password">
        <span>
    <?php 
        if (isset($_GET['Message'])) {   
            echo $_GET['Message'];
        }
    ?>
    </span>
    <span>
    <?php 
        if (isset($_GET['password'])) {
            echo $_GET['password'];
        }
    ?>
    </span>
    </div>
    <input type="submit" name="submit" value="submit">
    </form>
    <a href="view/register.php" title="register">Register now</a>
	</div>
	<script src="assets/js/script.js"></script>
</body>
</html>